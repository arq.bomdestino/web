import { ISolicitacao } from 'app/entities/solicitacao/solicitacao.model';

export interface IArea {
  id?: number;
  codArea?: string | null;
  descricao?: string | null;
  solicitacao?: ISolicitacao | null;
}

export class Area implements IArea {
  constructor(
    public id?: number,
    public codArea?: string | null,
    public descricao?: string | null,
    public solicitacao?: ISolicitacao | null
  ) {}
}

export function getAreaIdentifier(area: IArea): number | undefined {
  return area.id;
}
