import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IArea, Area } from '../area.model';
import { AreaService } from '../service/area.service';
import { ISolicitacao } from 'app/entities/solicitacao/solicitacao.model';
import { SolicitacaoService } from 'app/entities/solicitacao/service/solicitacao.service';

@Component({
  selector: 'jhi-area-update',
  templateUrl: './area-update.component.html',
})
export class AreaUpdateComponent implements OnInit {
  isSaving = false;

  solicitacaosCollection: ISolicitacao[] = [];

  editForm = this.fb.group({
    id: [],
    codArea: [],
    descricao: [],
    solicitacao: [],
  });

  constructor(
    protected areaService: AreaService,
    protected solicitacaoService: SolicitacaoService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ area }) => {
      this.updateForm(area);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const area = this.createFromForm();
    if (area.id !== undefined) {
      this.subscribeToSaveResponse(this.areaService.update(area));
    } else {
      this.subscribeToSaveResponse(this.areaService.create(area));
    }
  }

  trackSolicitacaoById(index: number, item: ISolicitacao): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IArea>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(area: IArea): void {
    this.editForm.patchValue({
      id: area.id,
      codArea: area.codArea,
      descricao: area.descricao,
      solicitacao: area.solicitacao,
    });

    this.solicitacaosCollection = this.solicitacaoService.addSolicitacaoToCollectionIfMissing(
      this.solicitacaosCollection,
      area.solicitacao
    );
  }

  protected loadRelationshipsOptions(): void {
    this.solicitacaoService
      .query({ filter: 'area-is-null' })
      .pipe(map((res: HttpResponse<ISolicitacao[]>) => res.body ?? []))
      .pipe(
        map((solicitacaos: ISolicitacao[]) =>
          this.solicitacaoService.addSolicitacaoToCollectionIfMissing(solicitacaos, this.editForm.get('solicitacao')!.value)
        )
      )
      .subscribe((solicitacaos: ISolicitacao[]) => (this.solicitacaosCollection = solicitacaos));
  }

  protected createFromForm(): IArea {
    return {
      ...new Area(),
      id: this.editForm.get(['id'])!.value,
      codArea: this.editForm.get(['codArea'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
      solicitacao: this.editForm.get(['solicitacao'])!.value,
    };
  }
}
