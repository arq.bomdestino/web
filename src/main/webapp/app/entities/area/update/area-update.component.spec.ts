jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { AreaService } from '../service/area.service';
import { IArea, Area } from '../area.model';
import { ISolicitacao } from 'app/entities/solicitacao/solicitacao.model';
import { SolicitacaoService } from 'app/entities/solicitacao/service/solicitacao.service';

import { AreaUpdateComponent } from './area-update.component';

describe('Area Management Update Component', () => {
  let comp: AreaUpdateComponent;
  let fixture: ComponentFixture<AreaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let areaService: AreaService;
  let solicitacaoService: SolicitacaoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AreaUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(AreaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AreaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    areaService = TestBed.inject(AreaService);
    solicitacaoService = TestBed.inject(SolicitacaoService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call solicitacao query and add missing value', () => {
      const area: IArea = { id: 456 };
      const solicitacao: ISolicitacao = { id: 43264 };
      area.solicitacao = solicitacao;

      const solicitacaoCollection: ISolicitacao[] = [{ id: 26129 }];
      jest.spyOn(solicitacaoService, 'query').mockReturnValue(of(new HttpResponse({ body: solicitacaoCollection })));
      const expectedCollection: ISolicitacao[] = [solicitacao, ...solicitacaoCollection];
      jest.spyOn(solicitacaoService, 'addSolicitacaoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ area });
      comp.ngOnInit();

      expect(solicitacaoService.query).toHaveBeenCalled();
      expect(solicitacaoService.addSolicitacaoToCollectionIfMissing).toHaveBeenCalledWith(solicitacaoCollection, solicitacao);
      expect(comp.solicitacaosCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const area: IArea = { id: 456 };
      const solicitacao: ISolicitacao = { id: 31464 };
      area.solicitacao = solicitacao;

      activatedRoute.data = of({ area });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(area));
      expect(comp.solicitacaosCollection).toContain(solicitacao);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Area>>();
      const area = { id: 123 };
      jest.spyOn(areaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ area });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: area }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(areaService.update).toHaveBeenCalledWith(area);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Area>>();
      const area = new Area();
      jest.spyOn(areaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ area });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: area }));
      saveSubject.complete();

      // THEN
      expect(areaService.create).toHaveBeenCalledWith(area);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Area>>();
      const area = { id: 123 };
      jest.spyOn(areaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ area });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(areaService.update).toHaveBeenCalledWith(area);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackSolicitacaoById', () => {
      it('Should return tracked Solicitacao primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackSolicitacaoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
