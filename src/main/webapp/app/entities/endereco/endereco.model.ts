import { IImovel } from 'app/entities/imovel/imovel.model';

export interface IEndereco {
  id?: number;
  cep?: string | null;
  logradouro?: string | null;
  complemento?: string | null;
  bairro?: string | null;
  localidade?: string | null;
  uf?: string | null;
  imovel?: IImovel | null;
}

export class Endereco implements IEndereco {
  constructor(
    public id?: number,
    public cep?: string | null,
    public logradouro?: string | null,
    public complemento?: string | null,
    public bairro?: string | null,
    public localidade?: string | null,
    public uf?: string | null,
    public imovel?: IImovel | null
  ) {}
}

export function getEnderecoIdentifier(endereco: IEndereco): number | undefined {
  return endereco.id;
}
