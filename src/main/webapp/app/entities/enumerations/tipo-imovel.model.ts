export enum TipoImovel {
  CASA = 'CASA',

  PREDIO = 'PREDIO',

  PROPRIEDADE_RURAL = 'PROPRIEDADE_RURAL',

  LOTE = 'LOTE',
}
