import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'endereco',
        data: { pageTitle: 'madmApp.endereco.home.title' },
        loadChildren: () => import('./endereco/endereco.module').then(m => m.EnderecoModule),
      },
      {
        path: 'imovel',
        data: { pageTitle: 'madmApp.imovel.home.title' },
        loadChildren: () => import('./imovel/imovel.module').then(m => m.ImovelModule),
      },
      {
        path: 'agenda',
        data: { pageTitle: 'madmApp.agenda.home.title' },
        loadChildren: () => import('./agenda/agenda.module').then(m => m.AgendaModule),
      },
      {
        path: 'area',
        data: { pageTitle: 'madmApp.area.home.title' },
        loadChildren: () => import('./area/area.module').then(m => m.AreaModule),
      },
      {
        path: 'imposto',
        data: { pageTitle: 'madmApp.imposto.home.title' },
        loadChildren: () => import('./imposto/imposto.module').then(m => m.ImpostoModule),
      },
      {
        path: 'solicitacao',
        data: { pageTitle: 'madmApp.solicitacao.home.title' },
        loadChildren: () => import('./solicitacao/solicitacao.module').then(m => m.SolicitacaoModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
