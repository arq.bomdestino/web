import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ImpostoService } from '../service/imposto.service';

import { ImpostoComponent } from './imposto.component';

describe('Imposto Management Component', () => {
  let comp: ImpostoComponent;
  let fixture: ComponentFixture<ImpostoComponent>;
  let service: ImpostoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ImpostoComponent],
    })
      .overrideTemplate(ImpostoComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ImpostoComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ImpostoService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.impostos?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
