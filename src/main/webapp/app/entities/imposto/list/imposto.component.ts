import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IImposto } from '../imposto.model';
import { ImpostoService } from '../service/imposto.service';
import { ImpostoDeleteDialogComponent } from '../delete/imposto-delete-dialog.component';

@Component({
  selector: 'jhi-imposto',
  templateUrl: './imposto.component.html',
})
export class ImpostoComponent implements OnInit {
  impostos?: IImposto[];
  isLoading = false;

  constructor(protected impostoService: ImpostoService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.impostoService.query().subscribe(
      (res: HttpResponse<IImposto[]>) => {
        this.isLoading = false;
        this.impostos = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IImposto): number {
    return item.id!;
  }

  delete(imposto: IImposto): void {
    const modalRef = this.modalService.open(ImpostoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.imposto = imposto;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
