import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { TipoImposto } from 'app/entities/enumerations/tipo-imposto.model';
import { IImposto, Imposto } from '../imposto.model';

import { ImpostoService } from './imposto.service';

describe('Imposto Service', () => {
  let service: ImpostoService;
  let httpMock: HttpTestingController;
  let elemDefault: IImposto;
  let expectedResult: IImposto | IImposto[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ImpostoService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      tipoImposto: TipoImposto.IPTU,
      valorDevido: 0,
      dataVencimento: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dataVencimento: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Imposto', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dataVencimento: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dataVencimento: currentDate,
        },
        returnedFromService
      );

      service.create(new Imposto()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Imposto', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          tipoImposto: 'BBBBBB',
          valorDevido: 1,
          dataVencimento: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dataVencimento: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Imposto', () => {
      const patchObject = Object.assign(
        {
          dataVencimento: currentDate.format(DATE_FORMAT),
        },
        new Imposto()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dataVencimento: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Imposto', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          tipoImposto: 'BBBBBB',
          valorDevido: 1,
          dataVencimento: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dataVencimento: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Imposto', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addImpostoToCollectionIfMissing', () => {
      it('should add a Imposto to an empty array', () => {
        const imposto: IImposto = { id: 123 };
        expectedResult = service.addImpostoToCollectionIfMissing([], imposto);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(imposto);
      });

      it('should not add a Imposto to an array that contains it', () => {
        const imposto: IImposto = { id: 123 };
        const impostoCollection: IImposto[] = [
          {
            ...imposto,
          },
          { id: 456 },
        ];
        expectedResult = service.addImpostoToCollectionIfMissing(impostoCollection, imposto);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Imposto to an array that doesn't contain it", () => {
        const imposto: IImposto = { id: 123 };
        const impostoCollection: IImposto[] = [{ id: 456 }];
        expectedResult = service.addImpostoToCollectionIfMissing(impostoCollection, imposto);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(imposto);
      });

      it('should add only unique Imposto to an array', () => {
        const impostoArray: IImposto[] = [{ id: 123 }, { id: 456 }, { id: 4193 }];
        const impostoCollection: IImposto[] = [{ id: 123 }];
        expectedResult = service.addImpostoToCollectionIfMissing(impostoCollection, ...impostoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const imposto: IImposto = { id: 123 };
        const imposto2: IImposto = { id: 456 };
        expectedResult = service.addImpostoToCollectionIfMissing([], imposto, imposto2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(imposto);
        expect(expectedResult).toContain(imposto2);
      });

      it('should accept null and undefined values', () => {
        const imposto: IImposto = { id: 123 };
        expectedResult = service.addImpostoToCollectionIfMissing([], null, imposto, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(imposto);
      });

      it('should return initial array if no Imposto is added', () => {
        const impostoCollection: IImposto[] = [{ id: 123 }];
        expectedResult = service.addImpostoToCollectionIfMissing(impostoCollection, undefined, null);
        expect(expectedResult).toEqual(impostoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
