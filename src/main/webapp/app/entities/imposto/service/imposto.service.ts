import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IImposto, getImpostoIdentifier } from '../imposto.model';

export type EntityResponseType = HttpResponse<IImposto>;
export type EntityArrayResponseType = HttpResponse<IImposto[]>;

@Injectable({ providedIn: 'root' })
export class ImpostoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/impostos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(imposto: IImposto): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(imposto);
    return this.http
      .post<IImposto>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(imposto: IImposto): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(imposto);
    return this.http
      .put<IImposto>(`${this.resourceUrl}/${getImpostoIdentifier(imposto) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(imposto: IImposto): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(imposto);
    return this.http
      .patch<IImposto>(`${this.resourceUrl}/${getImpostoIdentifier(imposto) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IImposto>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IImposto[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addImpostoToCollectionIfMissing(impostoCollection: IImposto[], ...impostosToCheck: (IImposto | null | undefined)[]): IImposto[] {
    const impostos: IImposto[] = impostosToCheck.filter(isPresent);
    if (impostos.length > 0) {
      const impostoCollectionIdentifiers = impostoCollection.map(impostoItem => getImpostoIdentifier(impostoItem)!);
      const impostosToAdd = impostos.filter(impostoItem => {
        const impostoIdentifier = getImpostoIdentifier(impostoItem);
        if (impostoIdentifier == null || impostoCollectionIdentifiers.includes(impostoIdentifier)) {
          return false;
        }
        impostoCollectionIdentifiers.push(impostoIdentifier);
        return true;
      });
      return [...impostosToAdd, ...impostoCollection];
    }
    return impostoCollection;
  }

  protected convertDateFromClient(imposto: IImposto): IImposto {
    return Object.assign({}, imposto, {
      dataVencimento: imposto.dataVencimento?.isValid() ? imposto.dataVencimento.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dataVencimento = res.body.dataVencimento ? dayjs(res.body.dataVencimento) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((imposto: IImposto) => {
        imposto.dataVencimento = imposto.dataVencimento ? dayjs(imposto.dataVencimento) : undefined;
      });
    }
    return res;
  }
}
