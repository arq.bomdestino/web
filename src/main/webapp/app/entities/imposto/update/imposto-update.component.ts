import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IImposto, Imposto } from '../imposto.model';
import { ImpostoService } from '../service/imposto.service';
import { IImovel } from 'app/entities/imovel/imovel.model';
import { ImovelService } from 'app/entities/imovel/service/imovel.service';
import { TipoImposto } from 'app/entities/enumerations/tipo-imposto.model';

@Component({
  selector: 'jhi-imposto-update',
  templateUrl: './imposto-update.component.html',
})
export class ImpostoUpdateComponent implements OnInit {
  isSaving = false;
  tipoImpostoValues = Object.keys(TipoImposto);

  imovelsSharedCollection: IImovel[] = [];

  editForm = this.fb.group({
    id: [],
    tipoImposto: [],
    valorDevido: [],
    dataVencimento: [],
    imovel: [],
  });

  constructor(
    protected impostoService: ImpostoService,
    protected imovelService: ImovelService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ imposto }) => {
      this.updateForm(imposto);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const imposto = this.createFromForm();
    if (imposto.id !== undefined) {
      this.subscribeToSaveResponse(this.impostoService.update(imposto));
    } else {
      this.subscribeToSaveResponse(this.impostoService.create(imposto));
    }
  }

  trackImovelById(index: number, item: IImovel): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IImposto>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(imposto: IImposto): void {
    this.editForm.patchValue({
      id: imposto.id,
      tipoImposto: imposto.tipoImposto,
      valorDevido: imposto.valorDevido,
      dataVencimento: imposto.dataVencimento,
      imovel: imposto.imovel,
    });

    this.imovelsSharedCollection = this.imovelService.addImovelToCollectionIfMissing(this.imovelsSharedCollection, imposto.imovel);
  }

  protected loadRelationshipsOptions(): void {
    this.imovelService
      .query()
      .pipe(map((res: HttpResponse<IImovel[]>) => res.body ?? []))
      .pipe(map((imovels: IImovel[]) => this.imovelService.addImovelToCollectionIfMissing(imovels, this.editForm.get('imovel')!.value)))
      .subscribe((imovels: IImovel[]) => (this.imovelsSharedCollection = imovels));
  }

  protected createFromForm(): IImposto {
    return {
      ...new Imposto(),
      id: this.editForm.get(['id'])!.value,
      tipoImposto: this.editForm.get(['tipoImposto'])!.value,
      valorDevido: this.editForm.get(['valorDevido'])!.value,
      dataVencimento: this.editForm.get(['dataVencimento'])!.value,
      imovel: this.editForm.get(['imovel'])!.value,
    };
  }
}
