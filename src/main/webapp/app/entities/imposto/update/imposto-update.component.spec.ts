jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ImpostoService } from '../service/imposto.service';
import { IImposto, Imposto } from '../imposto.model';
import { IImovel } from 'app/entities/imovel/imovel.model';
import { ImovelService } from 'app/entities/imovel/service/imovel.service';

import { ImpostoUpdateComponent } from './imposto-update.component';

describe('Imposto Management Update Component', () => {
  let comp: ImpostoUpdateComponent;
  let fixture: ComponentFixture<ImpostoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let impostoService: ImpostoService;
  let imovelService: ImovelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ImpostoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(ImpostoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ImpostoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    impostoService = TestBed.inject(ImpostoService);
    imovelService = TestBed.inject(ImovelService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Imovel query and add missing value', () => {
      const imposto: IImposto = { id: 456 };
      const imovel: IImovel = { id: 19717 };
      imposto.imovel = imovel;

      const imovelCollection: IImovel[] = [{ id: 47407 }];
      jest.spyOn(imovelService, 'query').mockReturnValue(of(new HttpResponse({ body: imovelCollection })));
      const additionalImovels = [imovel];
      const expectedCollection: IImovel[] = [...additionalImovels, ...imovelCollection];
      jest.spyOn(imovelService, 'addImovelToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ imposto });
      comp.ngOnInit();

      expect(imovelService.query).toHaveBeenCalled();
      expect(imovelService.addImovelToCollectionIfMissing).toHaveBeenCalledWith(imovelCollection, ...additionalImovels);
      expect(comp.imovelsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const imposto: IImposto = { id: 456 };
      const imovel: IImovel = { id: 44142 };
      imposto.imovel = imovel;

      activatedRoute.data = of({ imposto });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(imposto));
      expect(comp.imovelsSharedCollection).toContain(imovel);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Imposto>>();
      const imposto = { id: 123 };
      jest.spyOn(impostoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ imposto });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: imposto }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(impostoService.update).toHaveBeenCalledWith(imposto);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Imposto>>();
      const imposto = new Imposto();
      jest.spyOn(impostoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ imposto });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: imposto }));
      saveSubject.complete();

      // THEN
      expect(impostoService.create).toHaveBeenCalledWith(imposto);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Imposto>>();
      const imposto = { id: 123 };
      jest.spyOn(impostoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ imposto });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(impostoService.update).toHaveBeenCalledWith(imposto);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackImovelById', () => {
      it('Should return tracked Imovel primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackImovelById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
