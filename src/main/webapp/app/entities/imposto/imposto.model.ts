import * as dayjs from 'dayjs';
import { IImovel } from 'app/entities/imovel/imovel.model';
import { TipoImposto } from 'app/entities/enumerations/tipo-imposto.model';

export interface IImposto {
  id?: number;
  tipoImposto?: TipoImposto | null;
  valorDevido?: number | null;
  dataVencimento?: dayjs.Dayjs | null;
  imovel?: IImovel | null;
}

export class Imposto implements IImposto {
  constructor(
    public id?: number,
    public tipoImposto?: TipoImposto | null,
    public valorDevido?: number | null,
    public dataVencimento?: dayjs.Dayjs | null,
    public imovel?: IImovel | null
  ) {}
}

export function getImpostoIdentifier(imposto: IImposto): number | undefined {
  return imposto.id;
}
