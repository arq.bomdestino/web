import * as dayjs from 'dayjs';
import { ISolicitacao } from 'app/entities/solicitacao/solicitacao.model';

export interface IAgenda {
  id?: number;
  data?: dayjs.Dayjs | null;
  descricaoPrevia?: string | null;
  solicitacao?: ISolicitacao | null;
}

export class Agenda implements IAgenda {
  constructor(
    public id?: number,
    public data?: dayjs.Dayjs | null,
    public descricaoPrevia?: string | null,
    public solicitacao?: ISolicitacao | null
  ) {}
}

export function getAgendaIdentifier(agenda: IAgenda): number | undefined {
  return agenda.id;
}
