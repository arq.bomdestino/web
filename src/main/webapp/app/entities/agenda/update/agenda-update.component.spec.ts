jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { AgendaService } from '../service/agenda.service';
import { IAgenda, Agenda } from '../agenda.model';

import { AgendaUpdateComponent } from './agenda-update.component';

describe('Agenda Management Update Component', () => {
  let comp: AgendaUpdateComponent;
  let fixture: ComponentFixture<AgendaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let agendaService: AgendaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AgendaUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(AgendaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AgendaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    agendaService = TestBed.inject(AgendaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const agenda: IAgenda = { id: 456 };

      activatedRoute.data = of({ agenda });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(agenda));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Agenda>>();
      const agenda = { id: 123 };
      jest.spyOn(agendaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ agenda });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: agenda }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(agendaService.update).toHaveBeenCalledWith(agenda);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Agenda>>();
      const agenda = new Agenda();
      jest.spyOn(agendaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ agenda });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: agenda }));
      saveSubject.complete();

      // THEN
      expect(agendaService.create).toHaveBeenCalledWith(agenda);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Agenda>>();
      const agenda = { id: 123 };
      jest.spyOn(agendaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ agenda });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(agendaService.update).toHaveBeenCalledWith(agenda);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
