import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ImovelComponent } from './list/imovel.component';
import { ImovelDetailComponent } from './detail/imovel-detail.component';
import { ImovelUpdateComponent } from './update/imovel-update.component';
import { ImovelDeleteDialogComponent } from './delete/imovel-delete-dialog.component';
import { ImovelRoutingModule } from './route/imovel-routing.module';

@NgModule({
  imports: [SharedModule, ImovelRoutingModule],
  declarations: [ImovelComponent, ImovelDetailComponent, ImovelUpdateComponent, ImovelDeleteDialogComponent],
  entryComponents: [ImovelDeleteDialogComponent],
})
export class ImovelModule {}
