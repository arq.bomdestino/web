import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IImovel } from '../imovel.model';
import { ImovelService } from '../service/imovel.service';

@Component({
  templateUrl: './imovel-delete-dialog.component.html',
})
export class ImovelDeleteDialogComponent {
  imovel?: IImovel;

  constructor(protected imovelService: ImovelService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.imovelService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
