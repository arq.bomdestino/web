import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IImovel, Imovel } from '../imovel.model';
import { ImovelService } from '../service/imovel.service';

@Injectable({ providedIn: 'root' })
export class ImovelRoutingResolveService implements Resolve<IImovel> {
  constructor(protected service: ImovelService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IImovel> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((imovel: HttpResponse<Imovel>) => {
          if (imovel.body) {
            return of(imovel.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Imovel());
  }
}
