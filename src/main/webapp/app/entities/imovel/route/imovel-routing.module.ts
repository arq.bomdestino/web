import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ImovelComponent } from '../list/imovel.component';
import { ImovelDetailComponent } from '../detail/imovel-detail.component';
import { ImovelUpdateComponent } from '../update/imovel-update.component';
import { ImovelRoutingResolveService } from './imovel-routing-resolve.service';

const imovelRoute: Routes = [
  {
    path: '',
    component: ImovelComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ImovelDetailComponent,
    resolve: {
      imovel: ImovelRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ImovelUpdateComponent,
    resolve: {
      imovel: ImovelRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ImovelUpdateComponent,
    resolve: {
      imovel: ImovelRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(imovelRoute)],
  exports: [RouterModule],
})
export class ImovelRoutingModule {}
