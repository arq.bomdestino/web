import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IImovel, Imovel } from '../imovel.model';
import { ImovelService } from '../service/imovel.service';
import { IEndereco } from 'app/entities/endereco/endereco.model';
import { EnderecoService } from 'app/entities/endereco/service/endereco.service';
import { TipoImovel } from 'app/entities/enumerations/tipo-imovel.model';

@Component({
  selector: 'jhi-imovel-update',
  templateUrl: './imovel-update.component.html',
})
export class ImovelUpdateComponent implements OnInit {
  isSaving = false;
  tipoImovelValues = Object.keys(TipoImovel);

  enderecosCollection: IEndereco[] = [];

  editForm = this.fb.group({
    id: [],
    codImovel: [],
    documentoProprietario: [],
    tipoImovel: [],
    endereco: [],
  });

  constructor(
    protected imovelService: ImovelService,
    protected enderecoService: EnderecoService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ imovel }) => {
      this.updateForm(imovel);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const imovel = this.createFromForm();
    if (imovel.id !== undefined) {
      this.subscribeToSaveResponse(this.imovelService.update(imovel));
    } else {
      this.subscribeToSaveResponse(this.imovelService.create(imovel));
    }
  }

  trackEnderecoById(index: number, item: IEndereco): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IImovel>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(imovel: IImovel): void {
    this.editForm.patchValue({
      id: imovel.id,
      codImovel: imovel.codImovel,
      documentoProprietario: imovel.documentoProprietario,
      tipoImovel: imovel.tipoImovel,
      endereco: imovel.endereco,
    });

    this.enderecosCollection = this.enderecoService.addEnderecoToCollectionIfMissing(this.enderecosCollection, imovel.endereco);
  }

  protected loadRelationshipsOptions(): void {
    this.enderecoService
      .query({ filter: 'imovel-is-null' })
      .pipe(map((res: HttpResponse<IEndereco[]>) => res.body ?? []))
      .pipe(
        map((enderecos: IEndereco[]) =>
          this.enderecoService.addEnderecoToCollectionIfMissing(enderecos, this.editForm.get('endereco')!.value)
        )
      )
      .subscribe((enderecos: IEndereco[]) => (this.enderecosCollection = enderecos));
  }

  protected createFromForm(): IImovel {
    return {
      ...new Imovel(),
      id: this.editForm.get(['id'])!.value,
      codImovel: this.editForm.get(['codImovel'])!.value,
      documentoProprietario: this.editForm.get(['documentoProprietario'])!.value,
      tipoImovel: this.editForm.get(['tipoImovel'])!.value,
      endereco: this.editForm.get(['endereco'])!.value,
    };
  }
}
