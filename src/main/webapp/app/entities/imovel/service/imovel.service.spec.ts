import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TipoImovel } from 'app/entities/enumerations/tipo-imovel.model';
import { IImovel, Imovel } from '../imovel.model';

import { ImovelService } from './imovel.service';

describe('Imovel Service', () => {
  let service: ImovelService;
  let httpMock: HttpTestingController;
  let elemDefault: IImovel;
  let expectedResult: IImovel | IImovel[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ImovelService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      codImovel: 'AAAAAAA',
      documentoProprietario: 'AAAAAAA',
      tipoImovel: TipoImovel.CASA,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Imovel', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Imovel()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Imovel', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          codImovel: 'BBBBBB',
          documentoProprietario: 'BBBBBB',
          tipoImovel: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Imovel', () => {
      const patchObject = Object.assign(
        {
          documentoProprietario: 'BBBBBB',
          tipoImovel: 'BBBBBB',
        },
        new Imovel()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Imovel', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          codImovel: 'BBBBBB',
          documentoProprietario: 'BBBBBB',
          tipoImovel: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Imovel', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addImovelToCollectionIfMissing', () => {
      it('should add a Imovel to an empty array', () => {
        const imovel: IImovel = { id: 123 };
        expectedResult = service.addImovelToCollectionIfMissing([], imovel);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(imovel);
      });

      it('should not add a Imovel to an array that contains it', () => {
        const imovel: IImovel = { id: 123 };
        const imovelCollection: IImovel[] = [
          {
            ...imovel,
          },
          { id: 456 },
        ];
        expectedResult = service.addImovelToCollectionIfMissing(imovelCollection, imovel);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Imovel to an array that doesn't contain it", () => {
        const imovel: IImovel = { id: 123 };
        const imovelCollection: IImovel[] = [{ id: 456 }];
        expectedResult = service.addImovelToCollectionIfMissing(imovelCollection, imovel);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(imovel);
      });

      it('should add only unique Imovel to an array', () => {
        const imovelArray: IImovel[] = [{ id: 123 }, { id: 456 }, { id: 77727 }];
        const imovelCollection: IImovel[] = [{ id: 123 }];
        expectedResult = service.addImovelToCollectionIfMissing(imovelCollection, ...imovelArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const imovel: IImovel = { id: 123 };
        const imovel2: IImovel = { id: 456 };
        expectedResult = service.addImovelToCollectionIfMissing([], imovel, imovel2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(imovel);
        expect(expectedResult).toContain(imovel2);
      });

      it('should accept null and undefined values', () => {
        const imovel: IImovel = { id: 123 };
        expectedResult = service.addImovelToCollectionIfMissing([], null, imovel, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(imovel);
      });

      it('should return initial array if no Imovel is added', () => {
        const imovelCollection: IImovel[] = [{ id: 123 }];
        expectedResult = service.addImovelToCollectionIfMissing(imovelCollection, undefined, null);
        expect(expectedResult).toEqual(imovelCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
