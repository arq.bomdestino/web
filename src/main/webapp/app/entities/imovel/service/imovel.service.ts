import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IImovel, getImovelIdentifier } from '../imovel.model';

export type EntityResponseType = HttpResponse<IImovel>;
export type EntityArrayResponseType = HttpResponse<IImovel[]>;

@Injectable({ providedIn: 'root' })
export class ImovelService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/imovels');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(imovel: IImovel): Observable<EntityResponseType> {
    return this.http.post<IImovel>(this.resourceUrl, imovel, { observe: 'response' });
  }

  update(imovel: IImovel): Observable<EntityResponseType> {
    return this.http.put<IImovel>(`${this.resourceUrl}/${getImovelIdentifier(imovel) as number}`, imovel, { observe: 'response' });
  }

  partialUpdate(imovel: IImovel): Observable<EntityResponseType> {
    return this.http.patch<IImovel>(`${this.resourceUrl}/${getImovelIdentifier(imovel) as number}`, imovel, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IImovel>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IImovel[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addImovelToCollectionIfMissing(imovelCollection: IImovel[], ...imovelsToCheck: (IImovel | null | undefined)[]): IImovel[] {
    const imovels: IImovel[] = imovelsToCheck.filter(isPresent);
    if (imovels.length > 0) {
      const imovelCollectionIdentifiers = imovelCollection.map(imovelItem => getImovelIdentifier(imovelItem)!);
      const imovelsToAdd = imovels.filter(imovelItem => {
        const imovelIdentifier = getImovelIdentifier(imovelItem);
        if (imovelIdentifier == null || imovelCollectionIdentifiers.includes(imovelIdentifier)) {
          return false;
        }
        imovelCollectionIdentifiers.push(imovelIdentifier);
        return true;
      });
      return [...imovelsToAdd, ...imovelCollection];
    }
    return imovelCollection;
  }
}
