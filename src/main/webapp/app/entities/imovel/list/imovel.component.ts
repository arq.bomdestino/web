import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IImovel } from '../imovel.model';
import { ImovelService } from '../service/imovel.service';
import { ImovelDeleteDialogComponent } from '../delete/imovel-delete-dialog.component';

@Component({
  selector: 'jhi-imovel',
  templateUrl: './imovel.component.html',
})
export class ImovelComponent implements OnInit {
  imovels?: IImovel[];
  isLoading = false;

  constructor(protected imovelService: ImovelService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.imovelService.query().subscribe(
      (res: HttpResponse<IImovel[]>) => {
        this.isLoading = false;
        this.imovels = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IImovel): number {
    return item.id!;
  }

  delete(imovel: IImovel): void {
    const modalRef = this.modalService.open(ImovelDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.imovel = imovel;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
