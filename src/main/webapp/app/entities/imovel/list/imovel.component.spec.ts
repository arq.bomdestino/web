import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ImovelService } from '../service/imovel.service';

import { ImovelComponent } from './imovel.component';

describe('Imovel Management Component', () => {
  let comp: ImovelComponent;
  let fixture: ComponentFixture<ImovelComponent>;
  let service: ImovelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ImovelComponent],
    })
      .overrideTemplate(ImovelComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ImovelComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ImovelService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.imovels?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
