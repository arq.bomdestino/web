import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ImovelDetailComponent } from './imovel-detail.component';

describe('Imovel Management Detail Component', () => {
  let comp: ImovelDetailComponent;
  let fixture: ComponentFixture<ImovelDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ImovelDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ imovel: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ImovelDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ImovelDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load imovel on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.imovel).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
