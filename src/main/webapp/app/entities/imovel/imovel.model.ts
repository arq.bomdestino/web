import { IEndereco } from 'app/entities/endereco/endereco.model';
import { IImposto } from 'app/entities/imposto/imposto.model';
import { TipoImovel } from 'app/entities/enumerations/tipo-imovel.model';

export interface IImovel {
  id?: number;
  codImovel?: string | null;
  documentoProprietario?: string | null;
  tipoImovel?: TipoImovel | null;
  endereco?: IEndereco | null;
  impostos?: IImposto[] | null;
}

export class Imovel implements IImovel {
  constructor(
    public id?: number,
    public codImovel?: string | null,
    public documentoProprietario?: string | null,
    public tipoImovel?: TipoImovel | null,
    public endereco?: IEndereco | null,
    public impostos?: IImposto[] | null
  ) {}
}

export function getImovelIdentifier(imovel: IImovel): number | undefined {
  return imovel.id;
}
