import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISolicitacao, getSolicitacaoIdentifier } from '../solicitacao.model';

export type EntityResponseType = HttpResponse<ISolicitacao>;
export type EntityArrayResponseType = HttpResponse<ISolicitacao[]>;

@Injectable({ providedIn: 'root' })
export class SolicitacaoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/solicitacaos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(solicitacao: ISolicitacao): Observable<EntityResponseType> {
    return this.http.post<ISolicitacao>(this.resourceUrl, solicitacao, { observe: 'response' });
  }

  update(solicitacao: ISolicitacao): Observable<EntityResponseType> {
    return this.http.put<ISolicitacao>(`${this.resourceUrl}/${getSolicitacaoIdentifier(solicitacao) as number}`, solicitacao, {
      observe: 'response',
    });
  }

  partialUpdate(solicitacao: ISolicitacao): Observable<EntityResponseType> {
    return this.http.patch<ISolicitacao>(`${this.resourceUrl}/${getSolicitacaoIdentifier(solicitacao) as number}`, solicitacao, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISolicitacao>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISolicitacao[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addSolicitacaoToCollectionIfMissing(
    solicitacaoCollection: ISolicitacao[],
    ...solicitacaosToCheck: (ISolicitacao | null | undefined)[]
  ): ISolicitacao[] {
    const solicitacaos: ISolicitacao[] = solicitacaosToCheck.filter(isPresent);
    if (solicitacaos.length > 0) {
      const solicitacaoCollectionIdentifiers = solicitacaoCollection.map(solicitacaoItem => getSolicitacaoIdentifier(solicitacaoItem)!);
      const solicitacaosToAdd = solicitacaos.filter(solicitacaoItem => {
        const solicitacaoIdentifier = getSolicitacaoIdentifier(solicitacaoItem);
        if (solicitacaoIdentifier == null || solicitacaoCollectionIdentifiers.includes(solicitacaoIdentifier)) {
          return false;
        }
        solicitacaoCollectionIdentifiers.push(solicitacaoIdentifier);
        return true;
      });
      return [...solicitacaosToAdd, ...solicitacaoCollection];
    }
    return solicitacaoCollection;
  }
}
