import { IAgenda } from 'app/entities/agenda/agenda.model';
import { IArea } from 'app/entities/area/area.model';
import { TipoSolicitacao } from 'app/entities/enumerations/tipo-solicitacao.model';

export interface ISolicitacao {
  id?: number;
  codSolicitacao?: string | null;
  codUsuario?: string | null;
  tipoSolicitacao?: TipoSolicitacao | null;
  agenda?: IAgenda | null;
  area?: IArea | null;
}

export class Solicitacao implements ISolicitacao {
  constructor(
    public id?: number,
    public codSolicitacao?: string | null,
    public codUsuario?: string | null,
    public tipoSolicitacao?: TipoSolicitacao | null,
    public agenda?: IAgenda | null,
    public area?: IArea | null
  ) {}
}

export function getSolicitacaoIdentifier(solicitacao: ISolicitacao): number | undefined {
  return solicitacao.id;
}
