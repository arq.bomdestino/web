jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { SolicitacaoService } from '../service/solicitacao.service';
import { ISolicitacao, Solicitacao } from '../solicitacao.model';
import { IAgenda } from 'app/entities/agenda/agenda.model';
import { AgendaService } from 'app/entities/agenda/service/agenda.service';

import { SolicitacaoUpdateComponent } from './solicitacao-update.component';

describe('Solicitacao Management Update Component', () => {
  let comp: SolicitacaoUpdateComponent;
  let fixture: ComponentFixture<SolicitacaoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let solicitacaoService: SolicitacaoService;
  let agendaService: AgendaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [SolicitacaoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(SolicitacaoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SolicitacaoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    solicitacaoService = TestBed.inject(SolicitacaoService);
    agendaService = TestBed.inject(AgendaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call agenda query and add missing value', () => {
      const solicitacao: ISolicitacao = { id: 456 };
      const agenda: IAgenda = { id: 97333 };
      solicitacao.agenda = agenda;

      const agendaCollection: IAgenda[] = [{ id: 29227 }];
      jest.spyOn(agendaService, 'query').mockReturnValue(of(new HttpResponse({ body: agendaCollection })));
      const expectedCollection: IAgenda[] = [agenda, ...agendaCollection];
      jest.spyOn(agendaService, 'addAgendaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ solicitacao });
      comp.ngOnInit();

      expect(agendaService.query).toHaveBeenCalled();
      expect(agendaService.addAgendaToCollectionIfMissing).toHaveBeenCalledWith(agendaCollection, agenda);
      expect(comp.agendaCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const solicitacao: ISolicitacao = { id: 456 };
      const agenda: IAgenda = { id: 92020 };
      solicitacao.agenda = agenda;

      activatedRoute.data = of({ solicitacao });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(solicitacao));
      expect(comp.agendaCollection).toContain(agenda);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Solicitacao>>();
      const solicitacao = { id: 123 };
      jest.spyOn(solicitacaoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ solicitacao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: solicitacao }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(solicitacaoService.update).toHaveBeenCalledWith(solicitacao);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Solicitacao>>();
      const solicitacao = new Solicitacao();
      jest.spyOn(solicitacaoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ solicitacao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: solicitacao }));
      saveSubject.complete();

      // THEN
      expect(solicitacaoService.create).toHaveBeenCalledWith(solicitacao);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Solicitacao>>();
      const solicitacao = { id: 123 };
      jest.spyOn(solicitacaoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ solicitacao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(solicitacaoService.update).toHaveBeenCalledWith(solicitacao);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackAgendaById', () => {
      it('Should return tracked Agenda primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackAgendaById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
