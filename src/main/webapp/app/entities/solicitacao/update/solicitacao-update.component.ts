import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ISolicitacao, Solicitacao } from '../solicitacao.model';
import { SolicitacaoService } from '../service/solicitacao.service';
import { IAgenda } from 'app/entities/agenda/agenda.model';
import { AgendaService } from 'app/entities/agenda/service/agenda.service';
import { TipoSolicitacao } from 'app/entities/enumerations/tipo-solicitacao.model';

@Component({
  selector: 'jhi-solicitacao-update',
  templateUrl: './solicitacao-update.component.html',
})
export class SolicitacaoUpdateComponent implements OnInit {
  isSaving = false;
  tipoSolicitacaoValues = Object.keys(TipoSolicitacao);

  agendaCollection: IAgenda[] = [];

  editForm = this.fb.group({
    id: [],
    codSolicitacao: [],
    codUsuario: [],
    tipoSolicitacao: [],
    agenda: [],
  });

  constructor(
    protected solicitacaoService: SolicitacaoService,
    protected agendaService: AgendaService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ solicitacao }) => {
      this.updateForm(solicitacao);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const solicitacao = this.createFromForm();
    if (solicitacao.id !== undefined) {
      this.subscribeToSaveResponse(this.solicitacaoService.update(solicitacao));
    } else {
      this.subscribeToSaveResponse(this.solicitacaoService.create(solicitacao));
    }
  }

  trackAgendaById(index: number, item: IAgenda): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISolicitacao>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(solicitacao: ISolicitacao): void {
    this.editForm.patchValue({
      id: solicitacao.id,
      codSolicitacao: solicitacao.codSolicitacao,
      codUsuario: solicitacao.codUsuario,
      tipoSolicitacao: solicitacao.tipoSolicitacao,
      agenda: solicitacao.agenda,
    });

    this.agendaCollection = this.agendaService.addAgendaToCollectionIfMissing(this.agendaCollection, solicitacao.agenda);
  }

  protected loadRelationshipsOptions(): void {
    this.agendaService
      .query({ filter: 'solicitacao-is-null' })
      .pipe(map((res: HttpResponse<IAgenda[]>) => res.body ?? []))
      .pipe(map((agenda: IAgenda[]) => this.agendaService.addAgendaToCollectionIfMissing(agenda, this.editForm.get('agenda')!.value)))
      .subscribe((agenda: IAgenda[]) => (this.agendaCollection = agenda));
  }

  protected createFromForm(): ISolicitacao {
    return {
      ...new Solicitacao(),
      id: this.editForm.get(['id'])!.value,
      codSolicitacao: this.editForm.get(['codSolicitacao'])!.value,
      codUsuario: this.editForm.get(['codUsuario'])!.value,
      tipoSolicitacao: this.editForm.get(['tipoSolicitacao'])!.value,
      agenda: this.editForm.get(['agenda'])!.value,
    };
  }
}
